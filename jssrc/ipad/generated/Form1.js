function addWidgetsForm1() {
    Form1.setDefaultUnit(kony.flex.DP);
    var Button0g0b00c2aca4443 = new kony.ui.Button({
        "focusSkin": "defBtnFocus",
        "height": "50dp",
        "id": "Button0g0b00c2aca4443",
        "isVisible": true,
        "left": "108dp",
        "skin": "defBtnNormal",
        "text": "Button",
        "top": "130dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var Button0ic863c4b6d544f = new kony.ui.Button({
        "focusSkin": "defBtnFocus",
        "height": "50dp",
        "id": "Button0ic863c4b6d544f",
        "isVisible": true,
        "left": "239dp",
        "skin": "defBtnNormal",
        "text": "Button",
        "top": "43dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    Form1.add(Button0g0b00c2aca4443, Button0ic863c4b6d544f);
};

function Form1Globals() {
    Form1 = new kony.ui.Form2({
        "addWidgets": addWidgetsForm1,
        "enabledForIdleTimeout": false,
        "id": "Form1",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "slForm"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};